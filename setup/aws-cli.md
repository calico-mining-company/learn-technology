# AWS CLI

The command prompt interface for AWS. Very powerful and very worth learning.

First, [install it](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html) on whatever platform you use (Windows, Mac, Linux).


