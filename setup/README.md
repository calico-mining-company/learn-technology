# Setup

How to set up a developer's environment on your local computer!

## Table of Contents

+ Linux
+ Windows
+ MacOS
+ Chromebook

## Linux

My favorite OS for developing! You'll need to install some components that we'll use. 

+ Terraform
+ AWS CLI
+ gcloud SDK
+ so on

## Windows

Options:
+ WSL/Windows Subsystem for Linux
+ VSC/Visual Studio Code

### WSL/Windows Subsystem for Linux

Honestly, just install Windows Subsystem for Linux. It's really awesome that Microsoft made it. [Check it out here](https://docs.microsoft.com/en-us/windows/wsl/install-win10), then refer to the Linux section above. I would recommend installing Ubuntu 20.04 at the present moment (and whatever the latest LTS release is in the future).

### VSC/Visual Studio Code

I don't know much about dev on Windows otherwise, but lots of people are happy with Visual Studio Code from Microsoft, which would be your other solid option. I have nothing but good to say about it, except that I don't prefer it is all! 

## MacOS

If you're using a Mac, you have a lot of options as well. I prefer the terminal, but most use VSC/Visual Studio Code plus the terminal.

First, I'd [install Homebrew](https://brew.sh/), then I'd install some of the Linux apps that are missing from OSX (link to come later).

I have a handy script I can add later.
