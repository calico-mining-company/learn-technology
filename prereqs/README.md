# Prereqs 

Prerequisites that are the same in a few of the tutorials!

## Table of Contents

+ First Step: Domain Name
+ Password Manager
+ MFA
+ CloudFlare
+ GitLab
+ Make Cloud Accounts
  + Digital Ocean
  + GCP/Google Cloud Platform
  + AWS/Amazon Web Services

## First Step: Domain Name!

`Domain Name`: The thing you type to go to a website (ie `surfingdoggo.com`). 

You need a domain name to make a website! Know what you want it to be already? We're going to buy it!

Don't know yet? It's okay, buy one for like $2 (USD) that you can throw away later or just use for dev purposes. (Note: If you can't afford that, stop by a surfingdoggo stream on Twitch and I'm sure we can work something out)

Probably the easiest way to get a domain name is to use Google Domains. Everyone has a Google account these days, for better (or, more than likely) for worse, and it's a secure way to get a domain easy enough. We're going to transfer it to CloudFlare anyway (eventually, it's for convenience). 

I also use NameCheap.com, and I think they have more options and better deals. If you are going budget, I typically buy a `.xyz` domain for $2 or so.

`MFA`: Multi-Factor Authentication, which means when you log in, you have to verify it's you from another device. This way, if someone guesses your password, you're still very safe.

Note: Check out [haveibeenpwned](https://haveibeenpwned.com/) to see if any of your passwords have been leaked in data breaches anywhere! The odds are, they have *somewhere*. Don't reuse passwords, make sure you use pass phrases if possible, and always add symbols like `~&@#$*^&`!

Note: If you get hacked, you could lose your domain! If you get to the point of having significant web traffic to your website, you'll become a target, so we'll take steps to secure your domain name, including MFA (Multi Factor Authentication), also called 2FA, using a secure app (if you're familiar, text message MFA is not secure!).

Get started [here for Google](https://support.google.com/domains/answer/4491208?hl=en) or [here for NameCheap](https://www.namecheap.com/blog/how-to-buy-a-domain-name-dp/).

## Password Manager

I highly suggest pausing for a second to get a password manager. When you set up MFA below, you will be given backup codes, which you should guard as the *only* sure way to get into your account if all else fails.

I recommend KeePass (KeePassXC for Mac, and it depends on the day for Linux) to save passwords. It's a locally encrypted file that you can protect with a passphrase (note, use a non-common phrase with spaces and symbols) and even more options like a usb drive acting as a key. 

LastPass hasn't been hacked so far as we know, so that's probably a more user-friendly option.

Make sure, of course, you guard your password (phrase) to your password manager. Also, if you use KeePass, make sure you keep backup copies of your database every time you put in something important that you can't just reset. I stick mine in Google Drive and on a USB drive periodically (and after important passwords like MFA backup codes). This file can theoretically get corrupted, as can any file.

## MFA Using Authy

Note: DO NOT USE GOOGLE AUTH FOR MFA!!! If you lose your device, you lose your MFA codes.

Check out [Authy MFA Setup Instructions](https://authy.com/features/setup/). 

Let me know if you have any questions!

## Make a CloudFlare Account

`Server`: A fancy computer that serves up information (ie websites) when people (or other servers) ask nicely.

`DNS`: Domain Name System. Basically the address book and maps of the internet. When you type a website to visit, your browser asks what's called a `Name Server` where to go. It gives the directions to get to the server hosting your website.

CloudFlare is what we'll use for DNS for your website. 

`Server`: A fancy computer that serves up websites (among other things) when people visit them. All websites you visit run on servers.

`DNS`: The address book and maps of the internet. When you type a website to visit, your browser asks what's called a `Name Server` where to go. It gives the directions to get to the server hosting your website.

Go [here to create a CloudFlare account](https://support.cloudflare.com/hc/en-us/articles/201720164-Creating-a-Cloudflare-account-and-adding-a-website). This guide will walk you through creating an account and adding a website. For the paid tier, select free.

Note, also [secure your CloudFlare account with MFA](https://support.cloudflare.com/hc/en-us/articles/200167906-Securing-user-access-with-two-factor-authentication-2FA-)!

## GitLab

Gitlab is going to be important for reasons we'll get into. For now, [make a GitLab account](https://gitlab.com/users/sign_in#register-pane), and make sure you enable MFA!

Once you've made your account, don't forget to star this repository to bookmark it.

[This is a good guide](https://docs.gitlab.com/ee/intro/) on using GitLab. Don't worry too much about specifics, we'll get into it!

To learn more about `git`, check out our [../fundamentals](https://gitlab.com/calico-mining-company/learn-technology/-/tree/master/fundamentals#git) tutorial.

## Make Cloud Accounts

You'll need accounts with the main cloud providers. This is where you'll host your webapps. It's also where all the money is. Learn this and you can get a cushy job.

There are two main cloud providers, AWS, or Amazon Web Services, and GCP, or Google Cloud Platform. These are the two we'll focus on mainly. Microsoft also has a cloud platform called Azure. 

However, there is a smaller one called Digital Ocean, and we're going to start with this one. It only has a few features, but it is still powerful, and it's great for learning. 

### Make a Digital Ocean Account

Go [here to sign up for Digital Ocean](https://m.do.co/c/d262b3143b0e). You'll get $100 in free credit for 60 days if you follow this link (and, if you end up spending $25, I'll get a $25 credit on my bill, so thanks!). 

### Make a GCP Account

Coming soon!

### Make an AWS Account

AWS is the oldest and most widely used cloud platform.

This process will sound particularly daunting. We'll make it easy!

`IAM`: Identity Access Management, a very powerful way to manage accounts and permissions in the cloud, and is essential to building secure apps.

Note: This step can wait until you're more comfortable with this. You'll specifically need to set up IAM users which, while not *difficult*, requires a bit of background knowledge.

#### Before You Begin

The following is *crucial* information to understand before you begin this process. I cannot emphasize this enough. AWS is *extremely* powerful, and although you have to be pretty intentional to do so, you *can* incur enormous costs with it. The main concern is if your account is compromised, so if you use a password manager and follow the guides below, you'll be fine!

Before you begin the signup process, make sure you read all the information here thoroughly first and don't begin to signup unless you're sure you have time to finish the signup steps here, including adding MFA, making IAM users, setting budget alerts, and going through the security practices guide. 

I'll be releasing some videos about this as well. As always, prepare first, and don't get overwhelmed. There are plenty of resources to go to for help! 

[Read the Security Guide](https://docs.aws.amazon.com/IAM/latest/UserGuide/best-practices.html).

[Set Budget Alerts](https://aws.amazon.com/getting-started/hands-on/control-your-costs-free-tier-budgets/).

[Signup Guide](https://aws.amazon.com/premiumsupport/knowledge-center/create-and-activate-aws-account/).

[Enable MFA](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_mfa_enable_virtual.html).

## Set Up Your Dev Environment

Coming soon!
