# Cloud

General cloud computing concepts. 

For applied learning, see the `../coding` folder.

## Table of Contents

+Cloud Providers
  + AWS
  + GCP
  + Azure
  + Digital Ocean
+ Infrastructure as Code
  + Terraform
  + CloudFormation (AWS)
  + CDKs
  + CLI
+ Fundamental Cloud Concepts
  + PaaS
  + SaaS
  + IaaS

