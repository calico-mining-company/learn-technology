# Coding

Learn to code!

But learn by building cool things.

Note (more to self than anything): In this folder, we learn to code. For devopsy things, see ../devops, even though you'll learn to code there, too. IaC fits in here because it's focusing on the language itself, whereas in ../devops, it's focusing on the actual implementation of the implementation of ops components and cloud infrastructure (it's more meta). 

## Table of Contents

+ Python
+ Serverless
+ IaC/Infrastructure as Code
+ Cloud CLI/CDKs
+ Bash Scripting

## Python

## Serverless

Applied learning of serverless computing functions.

## IaC/Infrastrture as Code

## Cloud CLI/CDKs

## Bash scripting
