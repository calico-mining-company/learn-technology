# DevOps

Learning DevOps and implementing infrastructure components.

See [roadmap.sh](https://roadmap.sh/devops) for a map of the things we'll eventually learn. If you want to go on ahead, learn the recommended item for each category, and you'll get a job that pays $$$$$.

## Table of Contents

+ Cloud Environment Setup
