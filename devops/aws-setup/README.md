# AWS Setup

Initial setup for AWS.

## Table of Contents

## Prereqs

First, make sure you've done the prereqs and set up your AWS account.

## Environment Setup

Now, make sure you've gone through the [aws cli](https://gitlab.com/calico-mining-company/learn-technology/-/blob/master/setup/aws-cli.md) setup instructions and have run `aws configure` in the command line.

## IAM

Okay, it's going to be really important that you've read about IAM in [cloud](https://gitlab.com/calico-mining-company/learn-technology/-/tree/master/cloud). You can take my word for it if you want, but it's important stuff to know.

For now, go to the [AWS console](https://console.aws.amazon.com/?nc2=h_m_mc). In this guide, I'm assuming you're using the root account.

The only thing you should really use the AWS root account for is to make another account with limited permissions to use! The less often you use the root account, the better. 
