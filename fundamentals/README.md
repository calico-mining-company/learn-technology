# Fundamentals

Fundamentals that make things better.

## Table of Contents

+ Git
+ Dot File Notation

## Git

Git is awesome. It's how you manage code and versions and work on a team without stepping on each other's toes. The site you're on, GitLab, uses git.

`Version Control`:

`Git`:

## Dot File Notation

You'll see lots of references to dots. For example, in this repository, there is the `root` folder, found at [gitlab.com/calico-mining-company/learn-technology](https://gitlab.com/calico-mining-company/learn-technology), and it has some subdirectories, like `fundamentals`, `webstore`, and `prereqs`.

This is a little hard to explain without actually doing it! But we'll use this later on, so just store it away in the `freaking dots` section of your brain until it makes more practical sense.

The structure looks something like this:
root
|--fundamentals
|--webstore
|--prereqs

This is a little hard to explain, so my apologies for making this confusing. Let's say you click into `fundamentals`. 

Okay now pause. You have two ways to reference directories by dots:
`.`
`..`
So, one dot, or two dots. Neat, right?

`.` always refers to the current directory you are in, which right now is fundamentals.
`..` always refers to the *parent* directory, or the directory above it, which in this case is the `root` directory.

So now within `fundamentals`, there is this file you are currently reading, `README.md`. You can refer to it as `./README.md`. In this case, the `./` refers to *a file in the current working directory*, and in this case, `README.md`.

Now say you want to refer to the *root* README file. You can do that with `../README.md`. Confused? Good. I'm doing my job.

Now say you want to refer to `webstore` README file. You have to go up one directory, over one (to `webstore`), and down into it. You do this by going up one (`../`), typing the name of the directory (`webstore/`), and adding the filename `README.md`.

Now *why* is this useful? When you're writing code or just working in the command line, you'll always have a *current working directory*. You can change it to make your code more convenient, and it's useful to have a way to get back where you came from. 

In fact, you can nest the dots, ie `../../filename.ext`, which refers to a file located two directories above your current working directory.

##
