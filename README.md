## Learn technology!

In the meantime, start with [learning about GitLab](https://about.gitlab.com/what-is-gitlab/). If you're new to software entirely, `git` ([more here[1],](https://www.freecodecamp.org/news/what-is-git-learn-git-version-control/) [here[2],](https://www.git-scm.com/book/en/v2/Getting-Started-What-is-Git%3F) [and here[3]](https://en.wikipedia.org/wiki/Git)) is a way to work on a software project and organize all your code without going insane. It's something you use in the fancy hacker command line terminal. `GitLab` adds a nice interface and all kind of fancy features to view and automate code changes to save you time. We'll get more into it soon.

## Table of Contents

+ Cloud
+ Coding
+ Fundamentals
+ Prereqs
+ Webstore

## Cloud

General information on all things related to cloud computing.

## Coding

Fun, easy, applied learning to code!

## Fundamentals

Some fundamental knowledge that will make things make a lot more sense and make you work faster!

## Learn to Code a Web Store

Click into the webstore directory to learn to code by making a real life web store!

## Prereq Work

Some standard prerequisite work that is common to a lot of the tutorials, like making AWS accounts, security best practices, etc.
